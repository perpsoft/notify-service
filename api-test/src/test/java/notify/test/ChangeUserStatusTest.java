package notify.test;

import io.restassured.RestAssured;
import notify.api.ProjectConfig;
import notify.api.conditions.Condition;
import notify.api.conditions.Conditions;
import notify.api.payloads.UserStatusPayload;
import notify.api.services.UserStatusService;
import org.aeonbits.owner.ConfigFactory;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ChangeUserStatusTest {

    private final UserStatusService userStatusService = new UserStatusService();

    @BeforeClass
    public void setUp() {
        ProjectConfig config = ConfigFactory.create(ProjectConfig.class, System.getProperties());

        RestAssured.baseURI = config.baseUrl();
    }

    @Test
    public void testMakeUserIsActive() {

        UserStatusPayload userGlobalId = new UserStatusPayload()
                .globalId("1");

        userStatusService.makeActive(userGlobalId)
                .shouldHave(Conditions.statusCode(200))
                .shouldHave(Conditions.bodyField("status", Matchers.equalTo("ok")));
    }

    @Test
    public void testMakeUserNotActive() {

        UserStatusPayload userGlobalId = new UserStatusPayload()
                .globalId("1");

        userStatusService.makeNotActive(userGlobalId)
                .shouldHave(Conditions.statusCode(200))
                .shouldHave(Conditions.bodyField("status", Matchers.equalTo("ok")));
    }

    @Test
    public void testSendNoGlobalId() {

        UserStatusPayload userGlobalId = new UserStatusPayload();

        userStatusService.makeActive(userGlobalId)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")));

        userStatusService.makeNotActive(userGlobalId)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")));
    }

    @Test
    public void testSendWrongGlobalId() {

        UserStatusPayload userGlobalId = new UserStatusPayload()
                .globalId("wrongGlobalId");

        userStatusService.makeActive(userGlobalId)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")));

        userStatusService.makeNotActive(userGlobalId)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")));
    }
}
