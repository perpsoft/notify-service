package notify.test;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import notify.api.ProjectConfig;
import notify.api.conditions.Conditions;
import notify.api.payloads.MessagePayload;
import notify.api.services.MessageApiService;
import org.aeonbits.owner.ConfigFactory;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Locale;

/*Standart code

        String json =
        "{\n" +
        "  \"serviceName\": \"testService\",\n" +
        "  \"status\": \"warning\",\n" +
        "  \"users\": [\"1\", \"498593\"],\n" +
        "  \"title\": \"test title\",\n" +
        "  \"icon\": \"https://worksection.com/images/guide_iconset.png\",\n" +
        "  \"text\": \"this is test text\",\n" +
        "  \"dispatchTime\": \"Mon, 19 Dec 2020 17:28:35 GMT+2\",\n" +
        "  \"sendingServer\": [\"web-push\",\" telegram\", \"email\"] \n" +
        "}";

        RestAssured.given()
        .baseUri("http://localhost:3000/")
        .basePath("messages")
        .header("Content-Type", "application/json")
        .accept(ContentType.JSON)
        .body(json)
        .when().log().all()
        .post()
        .then()
        .statusCode(200)
        .body("status", Matchers.equalTo("ok"))
        .log().all().extract().response()
        *//*.prettyPrint()*//*;*/

public class MessageTest {
    //Faker - для шенерации данных автоматичесски
    // https://github.com/DiUS/java-faker
    private Faker faker;

    private final MessageApiService messageApiService = new MessageApiService();

    @BeforeClass
    public void setUp() {
        ProjectConfig config = ConfigFactory.create(ProjectConfig.class, System.getProperties());
        faker = new Faker(new Locale(config.locale()));

        RestAssured.baseURI = config.baseUrl();
    }

    @Test
    public void testCanSendMessageToUsers() {

        //given

        //MessagePayload генерируем с помощью плагина
        // RoboPojoGenerator https://github.com/robohorse/RoboPOJOGenerator
        MessagePayload message = new MessagePayload()
                .serviceName("test servicename")
                .sendingServer(Arrays.asList("telegram", "email"))
                .status("test status")
                .icon("test icon")
                .users(Arrays.asList("1", "498593"))
                //.groups(Arrays.asList("admin","user"))
                .dispatchTime("Mon, 19 Dec 2020 17:28:35 GMT+2")
                .text(faker.chuckNorris().fact())
                .title("test title");

        //expect
        messageApiService.sendMessage(message)
                .shouldHave(Conditions.statusCode(200))
                .shouldHave(Conditions.bodyField("status", Matchers.equalTo("ok")));
                /*.then().log().all()
                .assertThat()
                .statusCode(200)
                .body("status", Matchers.equalTo("ok"));*/

        //responses можем конвертировать обратно в классы
        //используя RoboPojoGenerator https://github.com/robohorse/RoboPOJOGenerator
        //
        /*MessageSentResponse response = messageApiService.sendMessage(message)
                .shouldHave(statusCode(200))
                .asPojo(MessageSentResponse.class);

        response.getStatus();*/
    }

    @Test
    public void testCanSendMessageToGroups() {

        MessagePayload message = new MessagePayload()
                .serviceName("test servicename")
                .sendingServer(Arrays.asList("telegram", "email"))
                .status("test status")
                .icon("test icon")
                //.users(Arrays.asList("1", "498593"))
                .groups(Arrays.asList("admin", "user"))
                .dispatchTime("Mon, 19 Dec 2020 17:28:35 GMT+2")
                .text(faker.chuckNorris().fact())
                .title("test title")
                ;

        messageApiService.sendMessage(message)
                .shouldHave(Conditions.statusCode(200))
                .shouldHave(Conditions.bodyField("status", Matchers.equalTo("ok")))
        ;
    }

    @Test
    public void testCanSendMessageToUserAndGroup() {

        MessagePayload message = new MessagePayload()
                .serviceName("test servicename")
                .sendingServer(Arrays.asList("telegram", "email"))
                .status("test status")
                .icon("test icon")
                .users(Arrays.asList("1"))
                .groups(Arrays.asList("user"))
                .dispatchTime("Mon, 19 Dec 2020 17:28:35 GMT+2")
                .text(faker.chuckNorris().fact())
                .title("test title")
                ;

        messageApiService.sendMessage(message)
                .shouldHave(Conditions.statusCode(200))
                .shouldHave(Conditions.bodyField("status", Matchers.equalTo("ok")))
        ;
    }

    @Test
    public void testCanSendMessageWithoutTitleAndIcon() {

        MessagePayload message = new MessagePayload()
                .serviceName("test servicename")
                .sendingServer(Arrays.asList("telegram", "email"))
                .status("test status")
                //.icon("test icon")
                .users(Arrays.asList("1", "498593"))
                //.groups(Arrays.asList("admin", "user"))
                .dispatchTime("Mon, 19 Dec 2020 17:28:35 GMT+2")
                .text(faker.chuckNorris().fact())
                //.title("test title")
                ;

        messageApiService.sendMessage(message)
                .shouldHave(Conditions.statusCode(200))
                .shouldHave(Conditions.bodyField("status", Matchers.equalTo("ok")))
        ;
    }

    @Test
    public void testCanNotSendEmptyMessage() {

        MessagePayload message = new MessagePayload()
                .serviceName("test servicename")
                .sendingServer(Arrays.asList("telegram", "email"))
                .status("test status")
                .icon("test icon")
                .users(Arrays.asList("1", "498593"))
                .groups(Arrays.asList("admin", "user"))
                .dispatchTime("Mon, 19 Dec 2020 17:28:35 GMT+2")
                //.text(faker.chuckNorris().fact())
                .title("test title")
                ;

        messageApiService.sendMessage(message)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")))
        ;
    }

    @Test
    public void testCanNotSendMessageWithoutSendingServer() {
        
        MessagePayload message = new MessagePayload()
                .serviceName("test servicename")
                //.sendingServer(Arrays.asList("telegram", "email"))
                .status("test status")
                .icon("test icon")
                .users(Arrays.asList("1", "498593"))
                .groups(Arrays.asList("admin", "user"))
                .dispatchTime("Mon, 19 Dec 2020 17:28:35 GMT+2")
                .text(faker.chuckNorris().fact())
                .title("test title")
                ;
        
        messageApiService.sendMessage(message)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")))
                ;
    }

    @Test
    public void testCanNotSendMessageWithoutServiceName() {

        MessagePayload message = new MessagePayload()
                //.serviceName("test servicename")
                .sendingServer(Arrays.asList("telegram", "email"))
                .status("test status")
                .icon("test icon")
                .users(Arrays.asList("1", "498593"))
                .groups(Arrays.asList("admin", "user"))
                .dispatchTime("Mon, 19 Dec 2020 17:28:35 GMT+2")
                .text(faker.chuckNorris().fact())
                .title("test title")
                ;

        messageApiService.sendMessage(message)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")))
        ;
    }

    @Test
    public void testCanNotSendMessageWithoutStatus() {

        MessagePayload message = new MessagePayload()
                .serviceName("test servicename")
                .sendingServer(Arrays.asList("telegram", "email"))
                //.status("test status")
                .icon("test icon")
                .users(Arrays.asList("1", "498593"))
                .groups(Arrays.asList("admin", "user"))
                .dispatchTime("Mon, 19 Dec 2020 17:28:35 GMT+2")
                .text(faker.chuckNorris().fact())
                .title("test title")
                ;

        messageApiService.sendMessage(message)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")))
        ;
    }

    @Test
    public void testCanNotSendMessageWithoutGroupAndUser() {

        MessagePayload message = new MessagePayload()
                .serviceName("test servicename")
                .sendingServer(Arrays.asList("telegram", "email"))
                .status("test status")
                .icon("test icon")
                //.users(Arrays.asList("1", "498593"))
                //.groups(Arrays.asList("admin", "user"))
                .dispatchTime("Mon, 19 Dec 2020 17:28:35 GMT+2")
                .text(faker.chuckNorris().fact())
                .title("test title")
                ;

        messageApiService.sendMessage(message)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")))
        ;
    }

    @Test
    public void testCanNotSendMessageWithoutDispatchTime() {

        MessagePayload message = new MessagePayload()
                .serviceName("test servicename")
                .sendingServer(Arrays.asList("telegram", "email"))
                .status("test status")
                .icon("test icon")
                .users(Arrays.asList("1", "498593"))
                .groups(Arrays.asList("admin", "user"))
                //.dispatchTime("Mon, 19 Dec 2020 17:28:35 GMT+2")
                .text(faker.chuckNorris().fact())
                .title("test title")
                ;

        messageApiService.sendMessage(message)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")))
        ;
    }

    @Test
    public void testCanNotSendMessageWithoutAllParametrs() {

        MessagePayload message = new MessagePayload()
                //.serviceName("test servicename")
                //.sendingServer(Arrays.asList("telegram", "email"))
                //.status("test status")
                //.icon("test icon")
                //.users(Arrays.asList("1", "498593"))
                //.groups(Arrays.asList("admin", "user"))
                //.dispatchTime("Mon, 19 Dec 2020 17:28:35 GMT+2")
                //.text(faker.chuckNorris().fact())
                //.title("test title")
                ;

        messageApiService.sendMessage(message)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")))
        ;
    }


}
