package notify.test;

import io.restassured.RestAssured;
import notify.api.ProjectConfig;
import notify.api.conditions.Conditions;
import notify.api.payloads.*;
import notify.api.services.UserOptionsApiService;
import org.aeonbits.owner.ConfigFactory;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;

public class ChangeUserOptionsTest {

    private final UserOptionsApiService userOptionsApiService = new UserOptionsApiService();

    @BeforeClass
    public void setUp() {
        ProjectConfig config = ConfigFactory.create(ProjectConfig.class, System.getProperties());

        RestAssured.baseURI = config.baseUrl();
    }

    @Test
    public void testCanChangeAllOptions() {

        UserOptionsPayload options = new UserOptionsPayload()
                .globalId("2")
                .userOptions(new UserAllOptions()
                        .timezone("UTC+2")
                        .deliveryChannels(Arrays.asList("telegram","email","websocket")));

        userOptionsApiService.changeOptions(options)
                .shouldHave(Conditions.statusCode(200))
                .shouldHave(Conditions.bodyField("status", Matchers.equalTo("ok")));
    }

    //!!!!!!!!!!!!!!!!!!!!!!
    //Стирает дату
    @Test
    public void testCanChangeOnlyTimeZone() {

        UserOptionTimezonePayload option = new UserOptionTimezonePayload()
                .globalId("2")
                .userOptionTimezone(new UserOptionTimezone()
                .timezone("UTC+2"));

        userOptionsApiService.changeOptionTimezone(option)
                .shouldHave(Conditions.statusCode(200))
                .shouldHave(Conditions.bodyField("status", Matchers.equalTo("ok")));
    }

    //!!!!!!!!!!!!!!!!!!!!!!
    //стирает ЗонуВремени
    @Test
    public void testCanChangeOnlyDeliveryChannels() {

        UserOptionDeliveryChannelsPayload option = new UserOptionDeliveryChannelsPayload()
                .globalId("2")
                .userOptionDeliveryChannels(new UserOptionDeliveryChannels()
                .deliveryChannels(Arrays.asList("telegram","email","websocket")));

        userOptionsApiService.changeOptionDeliveryTime(option)
                .shouldHave(Conditions.statusCode(200))
                .shouldHave(Conditions.bodyField("status", Matchers.equalTo("ok")));
    }

    @Test
    public void testCanChangeOptionsToNull() {

        UserOptionsPayload options = new UserOptionsPayload()
                .globalId("3")
                .userOptions(new UserAllOptions()
                        /*.timezone("UTC+2")
                        .deliveryChannels(Arrays.asList("telegram","email","websocket"))*/);

        userOptionsApiService.changeOptions(options)
                .shouldHave(Conditions.statusCode(200))
                .shouldHave(Conditions.bodyField("status", Matchers.equalTo("ok")));
    }

    @Test
    public void testCanNotChangeOptionsToNullGlobalIdUser() {

        UserOptionsPayload options = new UserOptionsPayload()
                .globalId(null)
                .userOptions(new UserAllOptions()
                        .timezone("UTC+2")
                        .deliveryChannels(Arrays.asList("telegram","email","websocket")));

        userOptionsApiService.changeOptions(options)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")));
    }
}
