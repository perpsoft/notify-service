package notify.test;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import notify.api.ProjectConfig;
import notify.api.conditions.Conditions;
import notify.api.payloads.IdPayload;
import notify.api.services.ViewApiService;
import org.aeonbits.owner.ConfigFactory;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Locale;

public class ViewMessageTest {

    private final ViewApiService viewApiService = new ViewApiService();

    @BeforeClass
    public void setUp() {
        ProjectConfig config = ConfigFactory.create(ProjectConfig.class, System.getProperties());

        RestAssured.baseURI = config.baseUrl();
    }

    @Test
    public void testCanMakeMessageView() {

        IdPayload idPayload = new IdPayload()
                .id("5d03ab57159c340fc4ea9747");

        viewApiService.sendId(idPayload)
                .shouldHave(Conditions.statusCode(200))
                .shouldHave(Conditions.bodyField("status", Matchers.equalTo("ok")));
    }

    @Test
    public void testCanNotSendNullId() {

        IdPayload idPayload = new IdPayload()
                .id(null);

        viewApiService.sendId(idPayload)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")));
    }

    @Test
    public void testCanNotSendWrongId() {

        IdPayload idPayload = new IdPayload()
                .id("thisIsWrongId");

        viewApiService.sendId(idPayload)
                .shouldHave(Conditions.statusCode(400))
                .shouldHave(Conditions.bodyField("error", Matchers.equalTo("Bad request")));
    }
}
