package notify.api.payloads;

//генерируем с помощью плагина RoboPojoGenerator https://github.com/robohorse/RoboPOJOGenerator

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.Generated;
import java.util.List;

@Getter
@Setter
@ToString
@Accessors(fluent = true)
@Generated("com.robohorse.robopojogenerator")
public class MessagePayload{

    ////конструктор опускаем, генерируем его автоматичесски с помощью плагина
    //    //lombok (@Getter , @Setter,
    //    @Accessors(fluent = true) - для редактирования значений через аннотации )

    @JsonProperty("dispatchTime")
    private String dispatchTime;

    @JsonProperty("icon")
    private String icon;

    @JsonProperty("groups")
    private List<String> groups;

    @JsonProperty("text")
    private String text;

    @JsonProperty("serviceName")
    private String serviceName;

    @JsonProperty("title")
    private String title;

    @JsonProperty("users")
    private List<String> users;

    @JsonProperty("status")
    private String status;

    @JsonProperty("sendingServer")
    private List<String> sendingServer;
}