package notify.api.payloads;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Generated;

@Getter
@Setter
@Accessors(fluent = true)
@Generated("com.robohorse.robopojogenerator")
public class UserOptionsPayload{

	@JsonProperty("globalId")
	private String globalId;

	@JsonProperty("userOptions")
	private UserAllOptions userOptions;
}