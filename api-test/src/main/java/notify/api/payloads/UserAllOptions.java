package notify.api.payloads;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Generated;

@Getter
@Setter
@Accessors(fluent = true)
@Generated("com.robohorse.robopojogenerator")
public class UserAllOptions{

	@JsonProperty("deliveryChannels")
	private List<String> deliveryChannels;

	@JsonProperty("timezone")
	private String timezone;
}