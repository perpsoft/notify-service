package notify.api;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:config.properties"})
public interface ProjectConfig extends Config {

    String env();

    @Key("test.baseUrl")
    String baseUrl();

    @Key("locale")
    @DefaultValue("en")
    String locale();

    @Key("logging")
    boolean logging();
}
