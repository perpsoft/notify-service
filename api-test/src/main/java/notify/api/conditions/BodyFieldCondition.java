package notify.api.conditions;

import io.restassured.response.Response;
import lombok.RequiredArgsConstructor;
import org.hamcrest.Matcher;

@RequiredArgsConstructor
public class BodyFieldCondition implements Condition {

    private final String jsonPath;
    private final Matcher matcher;

    //конструктор опускаем, генерируем его автоматичесски с помощью плагина
    //lombok (@RequiredArgsConstructor)
    /*public BodyFieldCondition(String jsonPath, Matcher matcher) {
        this.jsonPath = jsonPath;
        this.matcher = matcher;
    }*/

    @Override
    public void chek(Response response) {
        response.then().assertThat().body(jsonPath,matcher);
    }

    @Override
    public String toString() {
        return "body field [" + jsonPath + "] " + matcher;
    }
}