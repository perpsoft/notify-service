package notify.api.conditions;

import io.restassured.response.Response;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class StatusCodeCondition implements Condition {

    //статус код который проверяем из теста
    private final int statusCode;

    //конструктор опускаем, генерируем его автоматичесски с помощью плагина
    //lombok (@RequiredArgsConstructor)
    /*public StatusCodeCondition(int statusCode){
        this.statusCode = statusCode;
    }*/

    @Override
    public void chek(Response response) {
        response.then().assertThat().statusCode(statusCode);
    }

    @Override
    public String toString() {
        return "status code is " + statusCode;
    }
}