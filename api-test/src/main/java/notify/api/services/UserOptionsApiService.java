package notify.api.services;

import io.qameta.allure.Step;
import notify.api.assertions.AssertableResponse;
import notify.api.payloads.UserOptionDeliveryChannelsPayload;
import notify.api.payloads.UserOptionTimezonePayload;
import notify.api.payloads.UserOptionsPayload;

public class UserOptionsApiService extends ApiService {

    @Step
    public AssertableResponse changeOptions(UserOptionsPayload userOption){
        return new AssertableResponse(setup()
        .body(userOption)
        .when()
        .put("users/options"));
    }

    @Step
    public AssertableResponse changeOptionTimezone(UserOptionTimezonePayload userOptionTimezone){
        return new AssertableResponse(setup()
                .body(userOptionTimezone)
                .when()
                .put("users/options"));
    }

    @Step
    public AssertableResponse changeOptionDeliveryTime(UserOptionDeliveryChannelsPayload userOptionDeliveryChannels){
        return new AssertableResponse(setup()
                .body(userOptionDeliveryChannels)
                .when()
                .put("users/options"));
    }
}
