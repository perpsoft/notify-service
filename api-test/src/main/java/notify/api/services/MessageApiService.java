package notify.api.services;

import io.qameta.allure.Step;
import notify.api.assertions.AssertableResponse;
import notify.api.payloads.MessagePayload;

public class MessageApiService extends ApiService{

    @Step
    public AssertableResponse sendMessage(MessagePayload message) {
        return new AssertableResponse(setup()
                .body(message)
                .when()
                .post("messages") );
    }
}