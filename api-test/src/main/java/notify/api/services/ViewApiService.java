package notify.api.services;

import io.qameta.allure.Step;
import notify.api.assertions.AssertableResponse;
import notify.api.payloads.IdPayload;

public class ViewApiService extends ApiService {

    @Step
    public AssertableResponse sendId(IdPayload id){
        return new AssertableResponse(setup()
        .body(id)
        .when()
        .put("messages/view"));
    }
}
