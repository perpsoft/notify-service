package notify.api.services;

import io.qameta.allure.Step;
import notify.api.assertions.AssertableResponse;
import notify.api.payloads.UserStatusPayload;

public class UserStatusService extends ApiService {

    @Step
    public AssertableResponse makeActive(UserStatusPayload globalId){
        return new AssertableResponse(setup()
        .body(globalId)
        .when()
        .put("users/online/"));
    }

    @Step
    public AssertableResponse makeNotActive(UserStatusPayload globalId){
        return new AssertableResponse(setup()
                .body(globalId)
                .when()
                .put("users/offline/"));
    }
}
