package notify.api.services;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.filter.Filter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import notify.api.ProjectConfig;
import org.aeonbits.owner.ConfigFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ApiService {
    protected RequestSpecification setup() {

        return RestAssured
                .given().contentType(ContentType.JSON)
                //.log().all(); - включаем фильтры, что бы включать и отключать логи
                .filters(getFilters());
    }

    private List<Filter> getFilters(){
        ProjectConfig config = ConfigFactory.create(ProjectConfig.class, System.getProperties());
        //Boolean enable = Boolean.valueOf(System.getProperty("logging","true"));
        if (config.logging()) {
            return Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new AllureRestAssured());
        }
        return Collections.singletonList(new AllureRestAssured());
    }
}
