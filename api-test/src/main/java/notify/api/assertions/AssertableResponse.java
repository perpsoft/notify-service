package notify.api.assertions;

import io.qameta.allure.Step;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import notify.api.conditions.Condition;

@RequiredArgsConstructor
@Slf4j
public class AssertableResponse {

    private final Response response;

    //конструктор опускаем, генерируем его автоматичесски с помощью плагина
    //lombok (@RequiredArgsConstructor)
/*    public AssertableResponse(Response messages) {


        this.messages = messages;
}*/
    //Slf4j (lombok) для логирования (автоматичесски параметризуется

    @Step
    public AssertableResponse shouldHave(Condition condition) {
        log.info("About to check condition {}",condition);
        condition.chek(response);
        return this;
    }

    //генерировать ответ в класс с помощью POJO
    public <T> T asPojo(Class<T> tClass){
        return response.as(tClass);
    }

    //доставать хидэры
    public Headers headers(){
        return response.getHeaders();
    }
}